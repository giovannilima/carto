import 'package:carto/mapa/mapa_page_pais.dart';
import 'package:carto/mapa/widgets_regiao/regiao.dart';
import 'package:carto/router/scale.router.dart';
import 'package:flutter/material.dart';
import 'package:flare_flutter/flare_controls.dart';

class MapaMundi extends StatefulWidget {
  MapaMundi({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MapaMundiState createState() => _MapaMundiState();
}

class _MapaMundiState extends State<MapaMundi> {
  ScrollController scrollController;
  int cardIndex = 0;
  bool _isAnimate = false;
  final FlareControls animationControls = FlareControls();

  @override
  void initState() {
    super.initState();
    scrollController = new ScrollController();
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _isAnimate = !_isAnimate;
    });
    return Scaffold(
      body: Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("assets/images/fundo-home.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: ListView(
          physics: BouncingScrollPhysics(),
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    child: IconButton(
                      icon: Icon(
                        Icons.info_outline,
                        size: 30,
                        color: Colors.white,
                      ),
                      onPressed: () => Regiao.informacaoDados(context),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 140, 0, 0),
            ),
            Regiao.animacaoMapa("mapa_mundi", _isAnimate, 350, 350),
            Container(
              child: FloatingActionButton(
                backgroundColor: Colors.pink,
                child: Icon(
                  Icons.map,
                ),
                onPressed: () {
                  Navigator.push(context, ScaleRoute(page: MapaPagePais()));
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
