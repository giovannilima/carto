import 'dart:io';

import 'package:carto/mapa/estado/mapa_page_estado.dart';
import 'package:carto/mapa/model/dado_ibge.dart';
import 'package:carto/mapa/model/mapa_entity.dart';
import 'package:carto/router/scale.router.dart';
import 'package:carto/widgets/my_separator.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';

class Regiao {
  static double _padding = 5;
  static const colorRoxo = Color(0xFF3e3963);

  static Container regiaoPadraoEstado(
    context,
    bool _isAnimate,
    MapaEntity mapaEntity,
  ) {
    return Container(
      padding: EdgeInsets.fromLTRB(_padding, 120, _padding, 0),
      width: MediaQuery.of(context).size.width,
      child: Container(
        child: Stack(
          children: <Widget>[
            Container(
              margin: EdgeInsets.fromLTRB(_padding, 80, _padding, 10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  _nomeRegiao(mapaEntity.regiao),
                  _nomeMapaEstado(mapaEntity.nomeMapa),
                  _blocoInformacao(mapaEntity),
                  _blocoDadoInfoColumn(context, mapaEntity.capital),
                  _blocoDadoGentilicoColumn(context, mapaEntity.gentilico),
                  _blocoDadoInfoColumn(context, mapaEntity.comandante),
                  _blocoDadoInfo(
                    mapaEntity.habitantes,
                    "hab",
                    Icons.people,
                  ),
                  _blocoDadoInfo(
                    mapaEntity.area,
                    "km²",
                    Icons.terrain,
                  ),
                  _blocoDadoInfo(
                    mapaEntity.densidadeDemografica,
                    "hab/km²",
                    Icons.nature_people,
                  ),
                  MySeparator(color: colorRoxo),
                  _blocoDadoTempo(mapaEntity),
                ],
              ),
            ),
            animacaoMapa(mapaEntity.siglaMapa, _isAnimate, 250, 250),
          ],
        ),
      ),
    );
  }

  static _verificarAcessoInternet(context) async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        Navigator.push(context, ScaleRoute(page: MapaPageEstado()));
      }
    } on SocketException catch (_) {
      _semAcessoInternet(context);
    }
  }

  static Container regiaoPadraoPais(
    context,
    bool _isAnimate,
    MapaEntity mapaEntity,
  ) {
    return Container(
      padding: EdgeInsets.fromLTRB(_padding, 0, _padding, 0),
      width: MediaQuery.of(context).size.width,
      child: Container(
        child: Stack(
          children: <Widget>[
            Container(
              margin: EdgeInsets.fromLTRB(_padding, 80, _padding, 0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  _nomeMapaPais(mapaEntity.nomeMapa),
                  Container(
                    padding: EdgeInsets.fromLTRB(50, 50, 50, 0),
                    child: FloatingActionButton.extended(
                      onPressed: () {
                        _verificarAcessoInternet(context);
                      },
                      icon: Icon(Icons.map),
                      label: Text("Listar Estados"),
                    ),
                  ),
                ],
              ),
            ),
            animacaoMapa(mapaEntity.siglaMapa, _isAnimate, 250, 250),
          ],
        ),
      ),
    );
  }

  static Container _blocoDadoGentilicoColumn(context, String gentilico) {
    return Container(
      padding: EdgeInsets.fromLTRB(25, 25, 0, 0),
      child: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.fromLTRB(0, 0, 0, 3),
            child: Text(
              "Gentilico",
              style: TextStyle(
                fontSize: 13,
                color: colorRoxo,
              ),
            ),
          ),
          Row(
            children: <Widget>[
              Tooltip(
                message: gentilico ?? '...',
                child: Container(
                  width: MediaQuery.of(context).size.width * .85,
                  child: Text(
                    gentilico ?? '...',
                    style: TextStyle(
                      fontSize: 20,
                      color: colorRoxo,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  static Container _blocoDadoInfoColumn(context, DadoEstrutura dadoIBGE) {
    String textoDado = (dadoIBGE ?? DadoEstrutura()).dado ?? '...';
    String textoDadoCompleto = textoDado;
    if (textoDado.length > 30) {
      textoDado = "${textoDado.substring(0, 30)} ...";
    }
    return Container(
      margin: EdgeInsets.fromLTRB(25, 25, 10, 0),
      child: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.fromLTRB(0, 0, 0, 3),
            child: Text(
              (dadoIBGE ?? DadoEstrutura()).nome ?? '...',
              style: TextStyle(
                fontSize: 13,
                color: colorRoxo,
              ),
            ),
          ),
          Container(
            child: Row(
              children: <Widget>[
                Tooltip(
                  message: textoDadoCompleto,
                  child: Container(
                    //width: MediaQuery.of(context).size.width * .7,
                    child: Text(
                      textoDado,
                      style: TextStyle(
                        fontSize: 20,
                        color: colorRoxo,
                      ),
                    ),
                  ),
                ),
                _margemInfo(),
                _periodoDado((dadoIBGE ?? DadoEstrutura()).periodoPesquisa),
              ],
            ),
          ),
        ],
      ),
    );
  }

  static Container _margemInfo() {
    return Container(
      width: 5,
    );
  }

  static Container _blocoDadoInfo(
    DadoEstrutura dadoIBGE,
    String index,
    IconData iconData,
  ) {
    return Container(
      padding: EdgeInsets.fromLTRB(25, 25, 0, 0),
      child: Row(
        children: <Widget>[
          Icon(
            iconData,
            color: colorRoxo,
            size: 30,
          ),
          _margemInfo(),
          Text(
            ("${(dadoIBGE ?? DadoEstrutura()).dado ?? '...'}"),
            style: TextStyle(
              fontSize: 20,
              color: colorRoxo,
            ),
          ),
          Text(
            (" [${index ?? '...'}]"),
            style: TextStyle(
              color: colorRoxo,
            ),
          ),
          _margemInfo(),
          _periodoDado((dadoIBGE ?? DadoEstrutura()).periodoPesquisa),
        ],
      ),
    );
  }

  static Container _blocoInformacao(MapaEntity mapaEntity) {
    DadoEstrutura idh = mapaEntity.idh ?? DadoEstrutura();
    DadoEstrutura temperatura = mapaEntity.temperatura ?? DadoEstrutura();
    return Container(
        margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
        child: IntrinsicHeight(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              _blocoInformacaoEspecifico(
                "${temperatura.dado ?? '...'}º C",
                "TEMPERATURA",
                temperatura.periodoPesquisa,
              ),
              _verticalDivider(),
              _blocoInformacaoEspecifico(
                "${idh.dado ?? '...'}",
                idh.nome,
                idh.periodoPesquisa,
              ),
            ],
          ),
        ));
  }

  static Container _verticalDivider() {
    return Container(
      height: 25,
      width: 1,
      color: colorRoxo,
    );
  }

  static Expanded _blocoInformacaoEspecifico(
    String dado,
    String nome,
    String periodoDado,
  ) {
    return Expanded(
      child: Container(
        child: Column(
          children: <Widget>[
            Text(
              dado ?? '...',
              style: TextStyle(
                fontSize: 20,
                color: colorRoxo,
              ),
            ),
            Text(
              nome ?? '...',
              style: TextStyle(
                fontSize: 12,
                color: colorRoxo,
              ),
            ),
            _periodoDado(periodoDado),
          ],
        ),
      ),
    );
  }

  static Text _periodoDado(String periodoDado) {
    return Text(
      periodoDado ?? '...',
      style: TextStyle(
        fontSize: 10,
        color: Colors.pink,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  static Container _nomeRegiao(String nome) {
    return Container(
      alignment: Alignment.centerRight,
      padding: EdgeInsets.fromLTRB(0, 25, 0, 0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(20),
            topLeft: Radius.circular(20),
          ),
          color: Colors.teal,
        ),
        padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
        child: Text(
          nome ?? '...',
          style: TextStyle(
            color: Colors.white,
            fontSize: 15,
          ),
        ),
      ),
    );
  }

  static Container _nomeMapaPais(String nome) {
    return Container(
      padding: EdgeInsets.fromLTRB(0, 135, 0, 10),
      child: Center(
        child: Text(
          nome ?? '...',
          style: TextStyle(
            color: colorRoxo,
            fontSize: 35,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  static Container _nomeMapaEstado(String nome) {
    return Container(
      padding: EdgeInsets.fromLTRB(0, 35, 0, 10),
      child: Center(
        child: Text(
          nome ?? '...',
          style: TextStyle(
            color: colorRoxo,
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  static Row animacaoMapa(
      String nome, bool _isAnimate, double height, double width) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          height: height,
          width: width,
          alignment: FractionalOffset.topCenter,
          child: nome == "DF"
              ? Container(
                  color: Colors.redAccent,
                  width: 20,
                  height: 20,
                )
              : Stack(
                  children: <Widget>[
                    Positioned.fill(
                      child: FlareActor(
                        "assets/$nome.flr",
                        alignment: Alignment.center,
                        animation: _isAnimate ? "Untitled" : "",
                        fit: BoxFit.cover,
                      ),
                    ),
                  ],
                ),
        ),
      ],
    );
  }

  static Future<void> informacaoDados(context) async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: Container(
              padding: EdgeInsets.all(10),
              child: Center(
                child: Text(
                  'Referência dos dados',
                  style: TextStyle(
                    fontSize: 20,
                    color: colorRoxo,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            contentPadding: EdgeInsets.all(20),
            children: <Widget>[
              Container(
                child: Text(
                  '* IBGE',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.green,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                child: Text(
                  '* Openweathermap',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.orange,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          );
        });
  }

  static _blocoDadoTempo(MapaEntity mapaEntity) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              child: Column(
                children: <Widget>[
                  Icon(
                    Icons.opacity,
                    size: 100,
                    color: colorRoxo,
                  ),
                  Text(
                    "umidade",
                    style: TextStyle(
                      color: colorRoxo,
                    ),
                  ),
                  Text(
                    mapaEntity.umidade ?? '...',
                    style: new TextStyle(
                      fontSize: 42,
                      color: colorRoxo,
                    ),
                  ),
                  Text(
                    "%",
                    style: TextStyle(
                      color: colorRoxo,
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              child: Column(
                children: <Widget>[
                  Icon(
                    Icons.cloud,
                    size: 100,
                    color: colorRoxo,
                  ),
                  Text(
                    "vento",
                    style: TextStyle(
                      color: colorRoxo,
                    ),
                  ),
                  Text(
                    mapaEntity.velocidadeVento ?? '...',
                    style: new TextStyle(
                      fontSize: 42,
                      color: colorRoxo,
                    ),
                  ),
                  Text(
                    "m/s",
                    style: TextStyle(
                      color: colorRoxo,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  static _semAcessoInternet(context) async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            contentPadding: EdgeInsets.all(20),
            children: <Widget>[
              Icon(
                Icons.block,
                color: Colors.pink,
                size: 100,
              ),
              Center(
                child: Padding(
                  padding: EdgeInsets.all(15),
                  child: Text(
                    "É necessário acesso a internet!",
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
              RaisedButton(
                color: Colors.pink,
                child: Text(
                  "Ok",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                  ),
                ),
                onPressed: () => Navigator.pop(context),
              )
            ],
          );
        });
  }
}
