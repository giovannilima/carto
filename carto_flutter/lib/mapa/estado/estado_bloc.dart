import 'dart:async';

import 'package:carto/mapa/estado/Id_dado.dart';
import 'package:carto/mapa/estado/dados_constantes.dart';
import 'package:carto/mapa/estado/estado_event.dart';
import 'package:carto/mapa/estado/estado_service.dart';
import 'package:carto/mapa/model/dado_ibge.dart';
import 'package:carto/mapa/model/estado_entity.dart';
import 'package:carto/mapa/model/mapa_entity.dart';

import 'dart:convert';

import 'package:carto/mapa/model/temperatura_entity.dart';

const QUANTIDADE_MAX_ERROR_REQUEST = 3;

class EstadoBloc {
  List<MapaEntity> _mapaEntity = new List<MapaEntity>();

  final _mapaEntityStateController = StreamController<List<MapaEntity>>();

  StreamSink<List<MapaEntity>> get _inMapaEntity =>
      _mapaEntityStateController.sink;

  Stream<List<MapaEntity>> get estados => _mapaEntityStateController.stream;

  final _mapaEntityEventController = StreamController<EstadoEvent>();

  Sink<EstadoEvent> get estadosEventSink => _mapaEntityEventController.sink;

  EstadoService estadoService = new EstadoService();

  EstadoBloc() {
    _mapaEntityEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(EstadoEvent estadoEvent) {
    if (estadoEvent is RequestEvent) {
      List<EstadoEntity> estadoList;
      estadoService.buscarMapas().then((onValue) {
        Iterable list = json.decode(onValue.body);
        estadoList = list.map((model) => EstadoEntity.fromJson(model)).toList();
        popularLista(estadoList);
        _inMapaEntity.add(_mapaEntity);
      }).catchError((onError) {
        print("ESSE:::$onError");
      });
    }
  }

  void dispose() {
    _mapaEntityEventController.close();
    _mapaEntityStateController.close();
  }

  void buscarInformacoes(MapaEntity mapa, id, {requestError = 0}) {
    estadoService.buscarInformacoes(id).then((onValue) {
      Iterable list = json.decode(onValue.body);
      List<IdDado> idDadoList =
          list.map((model) => IdDado.fromJson(model)).toList();
      popularListaIdDado(idDadoList, mapa);
      _inMapaEntity.add(_mapaEntity);
    }).catchError((onError) {
      if (requestError <= QUANTIDADE_MAX_ERROR_REQUEST) {
        buscarInformacoes(mapa, id, requestError: requestError + 1);
      } else {
        print("Error ao consultar Informações com id = $id");
      }
    });
  }

  popularListaIdDado(List<IdDado> items, MapaEntity mapa) {
    for (var idDado in items) {
      switch (idDado.id.toString()) {
        case DadosEstado.AREAK2:
          mapa.area = DadoEstrutura(
            dado: idDado.dado,
            periodoPesquisa: idDado.ano,
          );
          break;
        case DadosEstado.CAPITAL:
          mapa.capital = DadoEstrutura(
            dado: idDado.dado,
            nome: "Capital",
            periodoPesquisa: idDado.ano,
          );
          break;
        case DadosEstado.HABITANTES:
          mapa.habitantes = DadoEstrutura(
            dado: idDado.dado,
            periodoPesquisa: idDado.ano,
          );
          break;
        case DadosEstado.IDH:
          mapa.idh = DadoEstrutura(
            dado: idDado.dado,
            nome: "IDH",
            periodoPesquisa: idDado.ano,
          );
          break;
        case DadosEstado.GOVERNADOR:
          mapa.comandante = DadoEstrutura(
            dado: idDado.dado,
            nome: "Governador",
            periodoPesquisa: idDado.ano,
          );
          break;
        case DadosEstado.DENSIDADE:
          mapa.densidadeDemografica = DadoEstrutura(
            dado: idDado.dado,
            periodoPesquisa: idDado.ano,
          );
          break;
      }
      _inMapaEntity.add(_mapaEntity);
    }
  }

  void buscarGentilico(MapaEntity mapa, id, {requestError = 0}) {
    estadoService.buscarGentilico(id).then((onValue) {
      Map model = json.decode(onValue.body);
      var gentilico = model['$id']['GENTILICO'];
      mapa.gentilico = gentilico;
      _inMapaEntity.add(_mapaEntity);
    }).catchError((onError) {
      if (requestError <= QUANTIDADE_MAX_ERROR_REQUEST) {
        buscarGentilico(mapa, id, requestError: requestError + 1);
      } else {
        print("Error ao consultar Gentilico com id = $id");
      }
    });
  }

  void buscarTemperatura(MapaEntity mapa, nome, {requestError = 0}) {
    estadoService.buscarTemperatura(nome).then((onValue) {
      Map model = json.decode(onValue.body);
      TemperaturaEntity temp = TemperaturaEntity.fromJson(model);
      mapa.temperatura = DadoEstrutura(
        nome: "TEMPERATURA",
        dado: temp.temperatura,
        periodoPesquisa: temp.hora,
      );
      mapa.umidade = temp.humidade;
      mapa.velocidadeVento = temp.vento;
      _inMapaEntity.add(_mapaEntity);
    }).catchError((onError) {
      if (requestError <= QUANTIDADE_MAX_ERROR_REQUEST) {
        buscarTemperatura(mapa, nome, requestError: requestError + 1);
      } else {
        print("Error ao consultar Temperatura com nome = $nome");
      }
    });
  }

  void popularLista(List<EstadoEntity> estadoList) {
    for (EstadoEntity item in estadoList) {
      MapaEntity mapa = criarMapa(item);
      buscarGentilico(mapa, item.id);
      buscarInformacoes(mapa, item.id);
      buscarTemperatura(mapa, item.nome);
      _mapaEntity.add(mapa);
      _inMapaEntity.add(_mapaEntity);
    }
  }

  criarMapa(item) {
    return MapaEntity(
      nomeMapa: item.nome,
      siglaMapa: item.sigla,
      regiao: item.regiao,
      gentilico: item.gentilico,
    );
  }
}
