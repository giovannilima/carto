import 'package:carto/mapa/estado/estado_bloc.dart';
import 'package:carto/mapa/estado/estado_event.dart';
import 'package:carto/mapa/model/mapa_entity.dart';
import 'package:carto/mapa/widgets_regiao/regiao.dart';
import 'package:carto/search/custon_search_delegate.dart';
import 'package:flutter/material.dart';

class MapaPageEstado extends StatefulWidget {
  MapaPageEstado({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MapaPageEstadoState createState() => _MapaPageEstadoState();
}

class _MapaPageEstadoState extends State<MapaPageEstado> {
  ScrollController scrollController;
  int cardIndex = 0;
  bool _isAnimate = false;
  final _bloc = EstadoBloc();

  @override
  void initState() {
    super.initState();
    scrollController = ScrollController();

    _bloc.estadosEventSink.add(RequestEvent());
  }

  List<MapaEntity> lista = List<MapaEntity>();

  @override
  Widget build(BuildContext context) {
    setState(() {
      _isAnimate = !_isAnimate;
    });

    return Scaffold(
      body: Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("assets/images/fundo-home.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: ListView(
          physics: BouncingScrollPhysics(),
          children: <Widget>[
            Container(
              margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
              child: Row(
                children: <Widget>[
                  Container(
                    alignment: Alignment.centerLeft,
                    child: IconButton(
                      icon: Icon(
                        Icons.chevron_left,
                        color: Colors.white,
                        size: 35,
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.centerRight,
                      child: IconButton(
                        icon: Icon(
                          Icons.search,
                          color: Colors.white,
                          size: 35,
                        ),
                        onPressed: () {
                          showSearch(
                            context: context,
                            delegate: CustomSearchDelegate(lista),
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 1100,
              width: MediaQuery.of(context).size.width,
              child: StreamBuilder(
                stream: _bloc.estados,
                builder: (BuildContext context,
                    AsyncSnapshot<List<MapaEntity>> snapshot) {
                  lista = snapshot.data ?? List<MapaEntity>();
                  return ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: (snapshot.data ?? List()).length,
                    controller: scrollController,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, position) {
                      return GestureDetector(
                        child: Regiao.regiaoPadraoEstado(
                          context,
                          _isAnimate,
                          snapshot.data[position],
                        ),
                        onHorizontalDragEnd: (details) {
                          if (details.velocity.pixelsPerSecond.dx > 0) {
                            if (cardIndex > 0) cardIndex--;
                          } else {
                            if (cardIndex <
                                (snapshot.data ?? List()).length - 1)
                              cardIndex++;
                          }
                          setState(() {
                            scrollController.animateTo(
                                (cardIndex) * MediaQuery.of(context).size.width,
                                duration: Duration(milliseconds: 600),
                                curve: Curves.easeOutBack);
                          });
                        },
                      );
                    },
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
