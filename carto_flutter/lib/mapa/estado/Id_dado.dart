class IdDado {
  int id;
  String dado;
  String ano;

  IdDado.fromJson(Map json) {
    id = json['id'];
    int indexRes1 = (List.from(json['res']).length ?? 0) - 1;
    if (indexRes1 >= 0) {
      ano = ((Map.from(json['res'][indexRes1]['res']) ?? Map()).keys ?? Iterable.empty()).last;
      dado = json['res'][indexRes1]['res'][ano];
    }
  }
}
