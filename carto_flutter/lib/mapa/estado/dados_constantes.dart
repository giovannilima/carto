class DadosEstado {
  static const String AREAK2 = "48980";
  static const String IDH = "30255";
  static const String HABITANTES = "25207";
  static const String CAPITAL = "48981";
  static const String GOVERNADOR = "62876";
  static const String DENSIDADE = "48982";
}
