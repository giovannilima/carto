import 'package:carto/mapa/estado/dados_constantes.dart';
import 'package:http/http.dart';

class EstadoService {
  
  Future<Response> buscarMapas() {
    return get('https://servicodados.ibge.gov.br/api/v1/localidades/estados');
  }  

  Future<Response> buscarGentilico(id) {
    return get('https://servicodados.ibge.gov.br/api/v1/biblioteca?aspas=3&coduf=$id');
  }

  Future<Response> buscarInformacoes(id) {
    return get('https://servicodados.ibge.gov.br/api/v1/pesquisas/indicadores/${DadosEstado.AREAK2}%7C${DadosEstado.CAPITAL}%7C${DadosEstado.HABITANTES}%7C${DadosEstado.IDH}%7C${DadosEstado.GOVERNADOR}%7C${DadosEstado.DENSIDADE}/resultados/$id');
  }

  Future<Response> buscarTemperatura(nome) {
    return get('https://openweathermap.org/data/2.5/find?q=$nome&type=like&sort=population&cnt=30&appid=b6907d289e10d714a6e88b30761fae22&_=1555981095450');
  }

}