class TemperaturaEntity {
  String temperatura;
  String humidade;
  String hora;
  String vento;

  TemperaturaEntity.fromJson(Map json) {
    Map item = List.from(json['list']).where((item) => item['sys']['country'] == 'BR').first;
    temperatura = format(item['main']['temp'] - 273.15).toString();
    humidade = item['main']['humidity'].toString();
    vento = item['wind']['speed'].toString();
    DateTime data= DateTime.fromMillisecondsSinceEpoch(item['dt'] * 1000);
    hora = "${data.day.toString()}/${data.month.toString()}/${data.year.toString()} ${data.hour.toString()}h ${data.minute.toString()}m";
  }

  String format(double n) {
    return n.toStringAsFixed(n.truncateToDouble() == n ? 0 : 2);
  }

}
