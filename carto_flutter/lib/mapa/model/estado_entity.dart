class EstadoEntity {
  int id;
  String sigla;
  String nome;
  String regiao;
  String gentilico;

  EstadoEntity.fromJson(Map json)
      : id = json['id'],
        sigla = json['sigla'],
        nome = json['nome'],
        regiao = json['regiao']['nome'];
}
