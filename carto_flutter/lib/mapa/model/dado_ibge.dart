class DadoEstrutura {
  String nome;
  String dado;
  String periodoPesquisa;

  DadoEstrutura({
    String dado,
    String periodoPesquisa,
    String nome,
  }) {
    this.dado = dado;
    this.periodoPesquisa = periodoPesquisa;
    this.nome = nome;
  }
}
