import 'dart:core';
import 'package:carto/mapa/model/dado_ibge.dart';

class MapaEntity {
  String nomeMapa;
  String siglaMapa;
  DadoEstrutura temperatura;
  DadoEstrutura habitantes;
  DadoEstrutura area;
  String regiao;
  DadoEstrutura idh;
  String gentilico;
  DadoEstrutura densidadeDemografica;
  DadoEstrutura comandante;
  DadoEstrutura capital;
  String velocidadeVento;
  String umidade;

  MapaEntity({
    nomeMapa,
    siglaMapa,
    temperatura,
    habitantes,
    area,
    regiao,
    idh,
    gentilico,
    densidadeDemografica,
    comandante,
    capital,
    velocidadeVento,
    umidade,
  }) {
    this.nomeMapa = nomeMapa;
    this.siglaMapa = siglaMapa;
    this.temperatura = temperatura;
    this.habitantes = habitantes;
    this.area = area;
    this.regiao = regiao;
    this.idh = idh;
    this.gentilico = gentilico;
    this.densidadeDemografica = densidadeDemografica;
    this.comandante = comandante;
    this.capital = capital;
    this.velocidadeVento = velocidadeVento;
    this.umidade = umidade;
  }
}
