import 'package:carto/mapa/model/mapa_entity.dart';
import 'package:carto/mapa/widgets_regiao/regiao.dart';
import 'package:flutter/material.dart';

class MapaPagePais extends StatefulWidget {
  MapaPagePais({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MapaPagePaisState createState() => _MapaPagePaisState();
}

class _MapaPagePaisState extends State<MapaPagePais> {
  ScrollController scrollController;
  int cardIndex = 0;
  bool _isAnimate = false;
  @override
  void initState() {
    super.initState();
    scrollController = new ScrollController();
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _isAnimate = !_isAnimate;
    });
    return Scaffold(
      body: Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("assets/images/fundo-home.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: ListView(
          physics: BouncingScrollPhysics(),
          children: <Widget>[
            Container(
              margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
              child: Row(
                children: <Widget>[
                  Container(
                    alignment: Alignment.centerLeft,
                    child: IconButton(
                      icon: Icon(
                        Icons.chevron_left,
                        color: Colors.white,
                        size: 35,
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 150,
            ),
            Container(
              height: MediaQuery.of(context).size.width,
              width: MediaQuery.of(context).size.width,
              child: Regiao.regiaoPadraoPais(
                context,
                _isAnimate,
                MapaEntity(
                  nomeMapa: "Brasil",
                  siglaMapa: "Brasil",
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
