import 'package:carto/mapa/model/mapa_entity.dart';
import 'package:carto/mapa/widgets_regiao/regiao.dart';
import 'package:flutter/material.dart';

class CustomSearchDelegate extends SearchDelegate {
  final List<MapaEntity> _palavras;

  CustomSearchDelegate(List<MapaEntity> palavras)
      : _palavras = palavras,
        super();

  @override
  List<Widget> buildActions(BuildContext context) {
    return null;
  }

  @override
  ThemeData appBarTheme(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return theme.copyWith(
        primaryColor: Colors.teal,
        primaryIconTheme: theme.primaryIconTheme,
        primaryColorBrightness: theme.primaryColorBrightness,
        primaryTextTheme: theme.primaryTextTheme,
        textTheme: theme.textTheme.copyWith(
            title: theme.textTheme.title
                .copyWith(color: theme.primaryTextTheme.title.color)));
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      tooltip: 'Sair',
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        this.close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    Iterable<MapaEntity> mapa = _palavras.where(
        (word) => word.nomeMapa.toLowerCase().startsWith(query.toLowerCase()));
    if (mapa.isEmpty) {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                this.query,
                style: Theme.of(context)
                    .textTheme
                    .display2
                    .copyWith(fontWeight: FontWeight.normal),
              ),
              Text(
                'Estado não encontrado',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.black45,
                ),
              ),
            ],
          ),
        ),
      );
    }
    this.query = mapa.first.nomeMapa;
    return Scaffold(
      body: Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("assets/images/fundo-home.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: ListView(
          physics: BouncingScrollPhysics(),
          children: <Widget>[
            Container(
              height: 1100,
              width: MediaQuery.of(context).size.width,
              child: Regiao.regiaoPadraoEstado(
                context,
                true,
                mapa.first,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final Iterable<MapaEntity> suggestions = _palavras.where(
        (word) => word.nomeMapa.toLowerCase().startsWith(query.toLowerCase()));
    List<String> result = List();
    for (var item in suggestions) {
      result.add(item.nomeMapa);
    }
    //calling wordsuggestion list
    return _WordSuggestionList(
      query: this.query,
      suggestions: result.toList(),
      onSelected: (String suggestion) {
        this.query = suggestion;
        showResults(context);
      },
    );
  }
}

class _WordSuggestionList extends StatelessWidget {
  const _WordSuggestionList({this.suggestions, this.query, this.onSelected});

  final List<String> suggestions;
  final String query;
  final ValueChanged<String> onSelected;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme.subhead;
    return ListView.builder(
      itemCount: suggestions.length,
      itemBuilder: (BuildContext context, int i) {
        final String suggestion = suggestions[i];
        return ListTile(
          leading: Icon(Icons.map),
          title: RichText(
            text: TextSpan(
              text: suggestion.substring(0, query.length),
              style: textTheme.copyWith(fontWeight: FontWeight.bold),
              children: <TextSpan>[
                TextSpan(
                  text: suggestion.substring(query.length),
                  style: textTheme,
                ),
              ],
            ),
          ),
          onTap: () {
            onSelected(suggestion);
          },
        );
      },
    );
  }
}
