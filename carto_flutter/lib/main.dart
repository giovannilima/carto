import 'package:carto/home/mapa_mundi.dart';
import 'package:carto/mapa/estado/mapa_page_estado.dart';
import 'package:carto/mapa/mapa_page_pais.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: "Condensed",
      ),
      home: MapaMundi(
        title: "Cartô",
      ),
      routes: <String, WidgetBuilder>{
        '/home': (BuildContext context) => MapaMundi(),
        '/estado': (BuildContext context) => MapaPageEstado(),
        '/pais': (BuildContext context) => MapaPagePais(),
      },
    );
  }
}
